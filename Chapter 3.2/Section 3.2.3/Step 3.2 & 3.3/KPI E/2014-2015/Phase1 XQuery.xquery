<result>
{
for $study_program.Hostkey in distinct-values(doc("2014-2015 XML_Excl_Teacher.xml")//Rows/Row/Study_program.Hostkey) 
let $program := doc("2014-2015 XML_Excl_Teacher.xml")//Rows/Row[(Study_program.Hostkey = $study_program.Hostkey)]
return
<study>
	<studyid>
		{
		$study_program.Hostkey
		}
	</studyid>
		{
		for $date in distinct-values($program/Date)
		let $day := $program[(Date = $date)]
		order by $date
		return if (some $nightcollege in $day/EleventhTwelfthHour satisfies (contains($nightcollege/text(), "1"))) then
			(if (some $nextmorningcollege in $day/following-sibling::*[1]/FirstSecondHour satisfies (contains($nextmorningcollege/text(), "1"))) then
				<answer>1</answer>
			else <answer>0</answer>)
		else <answer>0</answer>
		}
</study>
}
</result>