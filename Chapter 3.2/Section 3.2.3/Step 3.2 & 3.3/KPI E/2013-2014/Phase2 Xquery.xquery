<results>
	{
	for $study in doc("Phase1 Result.xml")/result/study
	return 
	<result>
		<studyid>
			{
			$study/studyid/text()
			}
		</studyid>
		<timesviolated>
			{
			sum($study/answer)
			}
		</timesviolated>
	</result>
	}
</results>