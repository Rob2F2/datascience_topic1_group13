<results>
{
for $studyid in doc("Phase1 Result.xml")/result/study/studyid 
return 
<result>
	<studyid>
		{
		doc("Phase1 Result.xml")/result/study[studyid = $studyid]/studyid/text()
		}
	</studyid>
	<totalcontactmoments>
		{
		doc("Phase1 Result.xml")/result/study[studyid = $studyid]/totalcontactmoments/text()
		}
	</totalcontactmoments>
	<totalcontactdays>
		{
		doc("Phase1 Result.xml")/result/study[studyid = $studyid]/totalcontactdays/text()
		}
	</totalcontactdays>
	<daysatleast4contact>
		{
		sum(doc("Phase1 Result.xml")/result/study[studyid = $studyid]/numdaysatleast4contact/atleast4)
		}
	</daysatleast4contact>
	<dayslessthan4contact>
		{
		(count(doc("Phase1 Result.xml")/result/study[studyid = $studyid]/numdaysatleast4contact/atleast4)-sum(doc("Phase1 Result.xml")/result/study[studyid = $studyid]/numdaysatleast4contact/atleast4))
		}
	</dayslessthan4contact>
</result>
}
</results>