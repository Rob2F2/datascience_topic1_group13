<result>
{
for $study_program.Hostkey in distinct-values(doc("2013-2014 XML_Excl_Teacher.xml")//Rows/Row/Study_program.Hostkey) 
let $totalcontactmoments := count(distinct-values(doc("2013-2014 XML_Excl_Teacher.xml")//Rows/Row[Study_program.Hostkey = $study_program.Hostkey and (Activitytype = "AMS" or "COL" or "HC" or "OVO" or "PJB" or "PJO" or "PRA" or "PRS" or "RESP" or "WC" or "ZMB")])),
    $totalcontactdays := ( ( distinct-values(doc("2013-2014 XML_Excl_Teacher.xml")//Rows/Row[Study_program.Hostkey = $study_program.Hostkey and (Activitytype = "AMS" or "COL" or "HC" or "OVO" or "PJB" or "PJO" or "PRA" or "PRS" or "RESP" or "WC" or "ZMB")]/Date))) 
return
<study>
	<studyid>
		{
		$study_program.Hostkey
		}
	</studyid>
	<totalcontactmoments>
		{
		$totalcontactmoments
		}
	</totalcontactmoments>
	<totalcontactdays>
		{
		count($totalcontactdays)
		}
	</totalcontactdays>
	<numdaysatleast4contact>
		{
		for $contactday in $totalcontactdays
		return 
			if( sum(doc("2013-2014 XML_Excl_Teacher.xml")//Rows/Row[(Study_program.Hostkey = $study_program.Hostkey and (Activitytype = "AMS" or "COL" or "HC" or "OVO" or "PJB" or "PJO" or "PRA" or "PRS" or "RESP" or "WC" or "ZMB")) and (Date = $contactday)]/Contact_hours) >= 4 ) then (: increase counter atleast 4, repr. by a 1 :)
				<atleast4>1</atleast4>
			else if( sum(doc("2013-2014 XML_Excl_Teacher.xml")//Rows/Row[(Study_program.Hostkey = $study_program.Hostkey and (Activitytype = "AMS" or "COL" or "HC" or "OVO" or "PJB" or "PJO" or "PRA" or "PRS" or "RESP" or "WC" or "ZMB")) and (Date = $contactday)]/Contact_hours) < 4  ) then (:increase counter less than 4, repr. by a 0:)
				<atleast4>0</atleast4>
			else 
				<atleast4> </atleast4> (:it is another type of activity so not defined as contact hour:)
		}
	</numdaysatleast4contact>
</study>
}
</result>