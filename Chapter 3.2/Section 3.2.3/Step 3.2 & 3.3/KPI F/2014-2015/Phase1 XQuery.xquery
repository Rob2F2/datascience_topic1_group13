<result> 
{ 
for $study_program.Hostkey in distinct-values(doc("2014-2015 XML_Excl_Teacher.xml")//Rows/Row/Study_program.Hostkey) 
let $totalfridaycontactmoments := distinct-values(doc("2014-2015 XML_Excl_Teacher.xml")//Rows/Row[(Study_program.Hostkey = $study_program.Hostkey) and (Day = "000000000000006") and (Activitytype = "AMS" or "COL" or "HC" or "OVO" or "PJB" or "PJO" or "PRA" or "PRS" or "RESP" or "WC" or "ZMB")]), 
	$totalfridaycontactdays := distinct-values(doc("2014-2015 XML_Excl_Teacher.xml")//Rows/Row[(Study_program.Hostkey = $study_program.Hostkey) and (Day = "000000000000006") and (Activitytype = "AMS" or "COL" or "HC" or "OVO" or "PJB" or "PJO" or "PRA" or "PRS" or "RESP" or "WC" or "ZMB")]/Date), 
	$totalfridayeveningclasses := sum(doc("2014-2015 XML_Excl_Teacher.xml")//Rows/Row[(Study_program.Hostkey = $study_program.Hostkey) and (Day = "000000000000006") and (Activitytype = "AMS" or "COL" or "HC" or "OVO" or "PJB" or "PJO" or "PRA" or "PRS" or "RESP" or "WC" or "ZMB")]/EveningClass),
	$totaluniquedaysthananfridayeveningclassoccurs := distinct-values(doc("2014-2015 XML_Excl_Teacher.xml")//Rows/Row[(Study_program.Hostkey = $study_program.Hostkey) and (Day = "000000000000006") and (Activitytype = "AMS" or "COL" or "HC" or "OVO" or "PJB" or "PJO" or "PRA" or "PRS" or "RESP" or "WC" or "ZMB") and (EveningClass = 1)]/Date)
return 
	<study> 
		<studyid> { $study_program.Hostkey } </studyid> 
		<totalfridaycontactmoments> { count($totalfridaycontactmoments) } </totalfridaycontactmoments> 
		<totalfridaycontactdays> { count($totalfridaycontactdays) } </totalfridaycontactdays> 
		<fridayeveningclasses> { $totalfridayeveningclasses } </fridayeveningclasses> 
		<non-eveningfridayclasses> { (count($totalfridaycontactmoments)-$totalfridayeveningclasses) } </non-eveningfridayclasses> 
		<totaluniquedaysthananfridayeveningclassoccurs>{ (count($totaluniquedaysthananfridayeveningclassoccurs))}</totaluniquedaysthananfridayeveningclassoccurs>
	</study> 
} 
</result>