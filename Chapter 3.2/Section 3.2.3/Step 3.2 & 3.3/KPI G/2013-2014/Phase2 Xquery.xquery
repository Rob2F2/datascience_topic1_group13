<results>
{
for $teacher in (doc("Phase1 Result.xml")/result/teacher/teacherid)
return 
<result>
	<teacherid>
		{
		$teacher
		}
	</teacherid>
	<totalcontactmoments>
		{
		doc("Phase1 Result.xml")/result/teacher[teacherid = $teacher]/totalcontactmoments/text()
		}
	</totalcontactmoments>
	<totalcontactdays>
		{
		doc("Phase1 Result.xml")/result/teacher[teacherid = $teacher]/totalcontactdays/text()
		}
	</totalcontactdays>
	<daysatmost8contact>
		{
		sum(doc("Phase1 Result.xml")/result/teacher[teacherid = $teacher]/numdaysatmost8contact/atmost8)
		}
	</daysatmost8contact>
	<dayshigherthan8contact>
		{
		count(doc("Phase1 Result.xml")/result/teacher[teacherid = $teacher]/numdaysatmost8contact/atmost8)-sum(doc("Phase1 Result.xml")/result/teacher[teacherid = $teacher]/numdaysatmost8contact/atmost8)
		}
	</dayshigherthan8contact>
</result>
}
</results>