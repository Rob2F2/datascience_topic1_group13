<result>
	{
	for $teacher in distinct-values(doc("2013-2014 XML_Incl_Teacher.xml")//Rows/Row/Teachernr)
	let $totalcontactmoments := count(distinct-values(doc("2013-2014 XML_Incl_Teacher.xml")//Rows/Row[(Teachernr = $teacher) and (((((((((((Activitytype = "AMS") or "COL") or "HC") or "OVO") or "PJB") or "PJO") or "PRA") or "PRS") or "RESP") or "WC") or "ZMB")])) , 
	    $totalcontactdays := distinct-values(doc("2013-2014 XML_Incl_Teacher.xml")//Rows/Row[(Teachernr = $teacher) and (((((((((((Activitytype = "AMS") or "COL") or "HC") or "OVO") or "PJB") or "PJO") or "PRA") or "PRS") or "RESP") or "WC") or "ZMB")]/Date)
	return 
	<teacher>
		<teacherid>
			{
			$teacher
			}
		</teacherid>
		<totalcontactmoments>
			{
			$totalcontactmoments
			}
		</totalcontactmoments>
		<totalcontactdays>
			{
			count($totalcontactdays)
			}
		</totalcontactdays>
		<numdaysatmost8contact>
			{
			for $contactday in $totalcontactdays 
			return 
				if( sum(doc("2013-2014 XML_Incl_Teacher.xml")//Rows/Row[((Teachernr = $teacher) and (((((((((((Activitytype = "AMS") or "COL") or "HC") or "OVO") or "PJB") or "PJO") or "PRA") or "PRS") or "RESP") or "WC") or "ZMB")) and (Date = $contactday)]/Contact_hours) <= 8 ) then(: increase counter atmost 6, repr. by a 1 :)
					<atmost8>1</atmost8>
				else
						if( sum(doc("2013-2014 XML_Incl_Teacher.xml")//Rows/Row[((Teachernr = $teacher) and (((((((((((Activitytype = "AMS") or "COL") or "HC") or "OVO") or "PJB") or "PJO") or "PRA") or "PRS") or "RESP") or "WC") or "ZMB")) and (Date = $contactday)]/Contact_hours) > 8 ) then(:increase counter higher than 6, repr. by a 0:)
							<atmost8>0</atmost8>
						else()
			}
		</numdaysatmost8contact>
	</teacher>
	}
</result>