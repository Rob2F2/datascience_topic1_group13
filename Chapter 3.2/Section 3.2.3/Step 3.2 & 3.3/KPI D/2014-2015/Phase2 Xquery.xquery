<results>
{
for $studyid in doc("Phase1 Result.xml")/result/study/studyid 
return 
<result>
	<studyid>
		{
		doc("Phase1 Result.xml")/result/study[studyid = $studyid]/studyid/text()
		}
	</studyid>
	<totalcontactmoments>
		{
		doc("Phase1 Result.xml")/result/study[studyid = $studyid]/totalcontactmoments/text()
		}
	</totalcontactmoments>
	<totalcontactdays>
		{
		doc("Phase1 Result.xml")/result/study[studyid = $studyid]/totalcontactdays/text()
		}
	</totalcontactdays>
	<daysatmost11hours>
		{
		sum(doc("Phase1 Result.xml")/result/study[studyid = $studyid]/atmost11collegehours/atmost11)
		}
	</daysatmost11hours>
	<dayshigherthan11hours>
		{
		(count(doc("Phase1 Result.xml")/result/study[studyid = $studyid]/atmost11collegehours/atmost11)-sum(doc("Phase1 Result.xml")/result/study[studyid = $studyid]/atmost11collegehours/atmost11))
		}
	</dayshigherthan11hours>
</result>
}
</results>