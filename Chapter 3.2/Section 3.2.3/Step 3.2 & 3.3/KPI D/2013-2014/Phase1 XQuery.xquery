<result>
	{
	for $study_program.Hostkey in distinct-values(doc("2013-2014 XML_Excl_Teacher.xml")//Rows/Row/Study_program.Hostkey) 
	let $totalcontactmoments := count(distinct-values(doc("2013-2014 XML_Excl_Teacher.xml")//Rows/Row[(Study_program.Hostkey = $study_program.Hostkey) and (((((((((((((Activitytype = "AMS") or "COL") or "HC") or "OVO") or "PJB") or "PJO") or "PRA") or "PRS") or "RESP") or "WC") or "ZMB") or "DTOETS") or "TOETS")])) , 
	    $totalcontactdays := ( ( distinct-values(doc("2013-2014 XML_Excl_Teacher.xml")//Rows/Row[(Study_program.Hostkey = $study_program.Hostkey) and (((((((((((((Activitytype = "AMS") or "COL") or "HC") or "OVO") or "PJB") or "PJO") or "PRA") or "PRS") or "RESP") or "WC") or "ZMB") or "DTOETS") or "TOETS")]/Date))) 
	return 
	<study>
		<studyid>
			{
			$study_program.Hostkey
			}
		</studyid>
		<totalcontactmoments>
			{
			$totalcontactmoments
			}
		</totalcontactmoments>
		<totalcontactdays>
			{
			count($totalcontactdays)
			}
		</totalcontactdays>
		<atmost11collegehours>
			{
			for $contactday in $totalcontactdays 
			return 
				if( sum(doc("2013-2014 XML_Excl_Teacher.xml")//Rows/Row[((Study_program.Hostkey = $study_program.Hostkey) and (((((((((((((Activitytype = "AMS") or "COL") or "HC") or "OVO") or "PJB") or "PJO") or "PRA") or "PRS") or "RESP") or "WC") or "ZMB") or "DTOETS") or "TOETS")) and (Date = $contactday)]/Contact_hours) <= 11 ) then(: increase counter atmost 11, repr. by a 1 :)
					<atmost11>1</atmost11>
				else
						if( sum(doc("2013-2014 XML_Excl_Teacher.xml")//Rows/Row[((Study_program.Hostkey = $study_program.Hostkey) and (((((((((((((Activitytype = "AMS") or "COL") or "HC") or "OVO") or "PJB") or "PJO") or "PRA") or "PRS") or "RESP") or "WC") or "ZMB") or "DTOETS") or "TOETS")) and (Date = $contactday)]/Contact_hours) > 11 ) then(:increase counter higher than 11, repr. by a 0:)
							<atmost11>0</atmost11>
						else
							<atmost11/>
			}
		</atmost11collegehours>
	</study>
	}
</result>