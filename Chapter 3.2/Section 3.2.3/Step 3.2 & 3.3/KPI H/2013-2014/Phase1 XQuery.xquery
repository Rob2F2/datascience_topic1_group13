<result>
	{
	for $teacherid in distinct-values(doc("2013-2014 XML_Incl_Teacher.xml")//Rows/Row/Teachernr)
	let $teacher := doc("2013-2014 XML_Incl_Teacher.xml")//Rows/Row[Teachernr = $teacherid]
	return 
	<teacher>
		<teacherid>
			{
			$teacher[1]/Teachernr/text()
			}
		</teacherid>
		<lastname>
			{
			$teacher[1]/Teacher-lastname/text()
			}
		</lastname>
		{
		for $date in distinct-values($teacher/Date)
		let $day := $teacher[(Date = $date)]
		order by $date
		return
			if( some $nightcollege in $day/EleventhTwelfthHour satisfies (contains($nightcollege/text(), "1")) ) then
				( if( some $nextmorningcollege in $day/following-sibling::*[1]/FirstSecondHour satisfies (contains($nextmorningcollege/text(), "1")) ) then
					<answer>1</answer> 
				else<answer>0</answer>)
			else
				<answer>0</answer>
		}
	</teacher>
	}
</result>