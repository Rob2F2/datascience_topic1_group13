<results>
	{
	for $teacherid in doc("Phase1 Result.xml")/result/teacher/teacherid
	let $teacher := doc("Phase1 Result.xml")/result/teacher[teacherid=$teacherid]
	return
	<result>
		<teacherid>
			{
			$teacher/teacherid
			}
		</teacherid>
		<timesviolated>
			{
			sum($teacher/answer/text())
			}
		</timesviolated>
	</result>
	}
</results>