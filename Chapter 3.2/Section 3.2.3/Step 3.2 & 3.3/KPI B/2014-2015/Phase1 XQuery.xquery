<result>
{
for $study_program.Hostkey in distinct-values(doc("2014-2015 XML_Excl_Teacher.xml")//Rows/Row/Study_program.Hostkey) 
let $totalcontactmoments := count(distinct-values(doc("2014-2015 XML_Excl_Teacher.xml")//Rows/Row[Study_program.Hostkey = $study_program.Hostkey and (Activitytype = "AMS" or "COL" or "HC" or "OVO" or "PJB" or "PJO" or "PRA" or "PRS" or "RESP" or "WC" or "ZMB")])),
    $totalcontactdays := ( ( distinct-values(doc("2014-2015 XML_Excl_Teacher.xml")//Rows/Row[Study_program.Hostkey = $study_program.Hostkey and (Activitytype = "AMS" or "COL" or "HC" or "OVO" or "PJB" or "PJO" or "PRA" or "PRS" or "RESP" or "WC" or "ZMB")]/Date))) 
return
<study>
	<studyid>
		{
		$study_program.Hostkey
		}
	</studyid>
	<totalcontactmoments>
		{
		$totalcontactmoments
		}
	</totalcontactmoments>
	<totalcontactdays>
		{
		count($totalcontactdays)
		}
	</totalcontactdays>
	<numdaysatmost6contact>
		{
		for $contactday in $totalcontactdays
		return 
			if( sum(doc("2014-2015 XML_Excl_Teacher.xml")//Rows/Row[(Study_program.Hostkey = $study_program.Hostkey and (Activitytype = "AMS" or "COL" or "HC" or "OVO" or "PJB" or "PJO" or "PRA" or "PRS" or "RESP" or "WC" or "ZMB")) and (Date = $contactday)]/Contact_hours) <= 6 ) then (: increase counter atmost 6, repr. by a 1 :)
				<atmost6>1</atmost6>
			else if( sum(doc("2014-2015 XML_Excl_Teacher.xml")//Rows/Row[(Study_program.Hostkey = $study_program.Hostkey and (Activitytype = "AMS" or "COL" or "HC" or "OVO" or "PJB" or "PJO" or "PRA" or "PRS" or "RESP" or "WC" or "ZMB")) and (Date = $contactday)]/Contact_hours) > 6  ) then (:increase counter higher than 6, repr. by a 0:)
				<atmost6>0</atmost6>
			else 
				<atmost6> </atmost6> (:it is another type of activity so not defined as contact hour:)
		}
	</numdaysatmost6contact>
</study>
}
</result>