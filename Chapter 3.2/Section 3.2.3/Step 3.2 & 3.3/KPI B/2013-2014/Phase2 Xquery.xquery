<results>
{
for $studyid in doc("Phase1 Result.xml")/result/study/studyid 
return 
<result>
	<studyid>
		{
		doc("Phase1 Result.xml")/result/study[studyid = $studyid]/studyid/text()
		}
	</studyid>
	<totalcontactmoments>
		{
		doc("Phase1 Result.xml")/result/study[studyid = $studyid]/totalcontactmoments/text()
		}
	</totalcontactmoments>
	<totalcontactdays>
		{
		doc("Phase1 Result.xml")/result/study[studyid = $studyid]/totalcontactdays/text()
		}
	</totalcontactdays>
	<daysatmost6contact>
		{
		sum(doc("Phase1 Result.xml")/result/study[studyid = $studyid]/numdaysatmost6contact/atmost6)
		}
	</daysatmost6contact>
	<dayshigherthan6contact>
		{
		(count(doc("Phase1 Result.xml")/result/study[studyid = $studyid]/numdaysatmost6contact/atmost6)-sum(doc("Phase1 Result.xml")/result/study[studyid = $studyid]/numdaysatmost6contact/atmost6))
		}
	</dayshigherthan6contact>
</result>
}
</results>