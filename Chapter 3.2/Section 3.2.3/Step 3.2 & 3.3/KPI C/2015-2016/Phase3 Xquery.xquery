<results>
	{
	for $study in doc("Phase2 Result.xml")/results/result
	return 
	<result>
		<studyid>
			{
			$study/studyid/text()
			}
		</studyid>
		{
		for $day in $study/date
		return if (sum($day/violated)>0) then
			(
			<day>
				{$day/Date}
				<violated>1</violated>
			</day>
			)
		else
			<day>
				{$day/Date}
				<violated>0</violated>
			</day>
		}
	</result>
	}
</results>