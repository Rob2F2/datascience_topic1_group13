<result>
{
for $study_program.Hostkey in distinct-values(doc("2014-2015 XML_Excl_Teacher.xml")//Rows/Row/Study_program.Hostkey) 
let $program := doc("2014-2015 XML_Excl_Teacher.xml")//Rows/Row[(Study_program.Hostkey = $study_program.Hostkey)]
return
<study>
	<studyid>
		{
		$study_program.Hostkey
		}
	</studyid>
		{
		for $date in distinct-values($program/Date)
		let $day := $program[(Date = $date)]
		return
		if (count($day) >= 2) then
			(<day>{$day[1]/Date}
			<start_times>{$day/Start_time_minutes}</start_times>,
			<end_times>{$day/End_time_minutes}</end_times>
			</day>)
		else ()
		}
</study>
}
</result>