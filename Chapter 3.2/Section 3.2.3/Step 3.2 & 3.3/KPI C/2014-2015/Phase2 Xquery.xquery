<results>
	{
	for $study in doc("Phase1 Result.xml")/result/study 
	return 
	<result>
		<studyid>
			{
			$study/studyid/text()
			}
		</studyid>
		{
		for $day in ( $study/day) 
		return 
		<date>
			{
			$day/Date
			}
			<violated>
				{
				for $event_start_times in ( $day/start_times/Start_time_minutes) , 
				    $event_end_times in ( $day/end_times/End_time_minutes) 
				let $difference := ($event_start_times[1]-$event_end_times[1])
				return if ($difference>120) then
					<value>1</value>
				else 
					<value>0</value>
				}
			</violated>
		</date>
		}
	</result>
	}
</results>