<result>
	<totaleducationalweeks>
		{
		doc('Phase1 Result.xml')/result/totaleducationalweeks/text()
		}
	</totaleducationalweeks>
	<totalrooms>
		{
		doc('Phase1 Result.xml')/result/totalrooms/text()
		}
	</totalrooms>
	{
	for $week in doc('Phase1 Result.xml')/result/educational_week 
	return 
	<educationalweek>
		<week>
			{
			$week/week
			}
		</week>
		{
		for $roomnr in $week/rooms_occupied/answer 
		return 
		<room>
			{
			<roomnr>{$roomnr/room/text()}</roomnr>,
			<timesoccupied>{sum($roomnr/week)}</timesoccupied>
			}
		</room>
		}
	</educationalweek>
	}
</result>