declare namespace exslt = "http://exslt.org/dates-and-times";
declare namespace date = "http://noexslt.org/dates-and-times";
declare namespace functx = "http://www.functx.com";
declare variable $date-time := "2010-01-02T00:00:00Z";
declare variable $month-lengths := (0, 31, 28, 31, 30, 31, 30, 31, 31,
30, 31, 30, 31);

declare function date:week-in-year( $date-time as xs:dateTime ) as xs:integer
{
    let $year := fn:year-from-dateTime( $date-time )
    let $day := fn:day-from-dateTime( $date-time )
    let $month := fn:month-from-dateTime( $date-time )

    let $days := sum( subsequence( $month-lengths, 1, $month ) )
    let $is-leap := ($year mod 4 = 0 and $year mod 100 != 0) or $year
mod 400 = 0
    return date:_week-in-year($year, $days + $day + (if ($is-leap and
$month > 2) then 1 else 0))
};

declare function date:_week-in-year( $year as xs:integer, $month-days
as xs:integer) as xs:integer
{
    let $previous-year := $year - 1
    let $is-leap := ($year mod 4 = 0 and $year mod 100 != 0) or $year
mod 400 = 0
    let $dow := ($previous-year + floor($previous-year div 4) -
        floor($previous-year div 100) + floor($previous-year div 400) +
        $month-days) mod 7
    let $day-of-week := if ($dow > 0) then $dow else 7
    let $start-day := ($month-days - $day-of-week + 7) mod 7
    let $week-number := floor(($month-days - $day-of-week + 7) div 7)
cast as xs:integer
    return
        if ($start-day >= 4) then $week-number + 1
        else if ($week-number = 0) then
            let $leap-day := if ((not($previous-year mod 4) and
$previous-year mod 100) or not($previous-year mod 400)) then 1 else 0
            return date:_week-in-year( $previous-year, 365 + $leap-day )
            else $week-number
};

declare function functx:is-value-in-sequence
  ( $value as xs:anyAtomicType? ,
    $seq as xs:anyAtomicType* )  as xs:boolean {

   $value = $seq
 } ;

declare variable $educational-weeks := (36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 2, 3, 4, 5, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27);
<result>
	<totaleducationalweeks>
		{
		count(distinct-values($educational-weeks))
		}
	</totaleducationalweeks>
	<totalrooms>
		{
		count(distinct-values(doc("2014-2015 XML_Excl_Teacher.xml")//Rows/Row[Room!=""]/Room))
		}
	</totalrooms>
	{
	for $week in $educational-weeks
	return
	<educational_week>
		<week>{$week}</week>
		<rooms_occupied>
			{
			for $room in distinct-values(doc("2014-2015 XML_Excl_Teacher.xml")//Rows/Row[exists(Date)]/Room)
			let $roominfo := (doc("2014-2015 XML_Excl_Teacher.xml")//Rows/Row[(Room = $room)]),
				$roomdates := ($roominfo[Date!=""]/Date)
			return
				<answer>
						<room>{$room}</room>
						{
						(:notice that we cannot simply create one if statement: (functx:is-value-in-sequence(date:week-in-year(dateTime(xs:date(distinct-values($roomdates)),xs:time('00:00:00-00:00'))),$week))
						because the date:week-in-year function does not work with this:)
						for $occupiedweeknrs in distinct-values($roomdates)
						let $weeknr := date:week-in-year(dateTime(xs:date($occupiedweeknrs),xs:time('00:00:00-00:00')))
						return if(functx:is-value-in-sequence($weeknr,$week)) then
							<week>1</week>
						else
							<week>0</week>
						}
				</answer>
			}
		</rooms_occupied>
	</educational_week>
	}
</result>