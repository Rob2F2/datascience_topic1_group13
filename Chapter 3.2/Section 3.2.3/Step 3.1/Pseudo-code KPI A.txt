//Pseudo-code KPI A\\

//Phase 1:

for each unique study program
let $totalcontactmoments := total college activities of the unique study program
let $totalcontactdays := total unique contact days of the unique study program
|	return{
|		unique study program hostkey,
|		$totalcontactmoments,
|		$totalcontactdays,
|		for each $contactday in $totalcontactdays
|		|	return{
|		|		1 if the $contactday contains at least four contact hours
|		|		0 otherwise
|		|		}
|		}

//use result of this query for next phase

//Phase 2:
for each unique study program from phase 1
|	return{
|		unique study program hostkey,
|		$totalcontactmoments,
|		$totalcontactdays,
|		sum of the total $contactdays in $totalcontactdays which are equal to 1,
|		1 - sum of the total $contactdays in $totalcontactdays which are equal to 1,
|		}