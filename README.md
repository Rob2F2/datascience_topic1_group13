# README #

### Data Science Topic 1 Group 13 ###
### Consist of Rob Bemthuis and Giel de Visser ###

This README document contains some general information and information about how to run the queries

### Which document types are included? ###
.doc, .xlsx, .pdf, .txt, .xml, .xquery, .png, .ktr, .md, .ppt, .twb, .fig

### What is this repository for? ###

* Quick summary
This repository contains the results from each step of the Timetabling Project 
 for the UT and University of Applied Sciences Saxion. Within the report, 
 which can be found in the main directory, we discuss our methodology. 

The methodology consists of three phases with steps:

<<<<<<< HEAD
1. Identify Information Needed
=======
####1. Identify Information Needed####
>>>>>>> 7b09c48e908bd5c9ba7193e3b146f1ab39f9ed71

1.1. Define KPIs

1.2. Specify the required information in words

1.3. Specify and consider dataset

1.4. Specify required operational data for KPIs

(notice that steps 1.2 till 1.4 are performed iteratively)

<<<<<<< HEAD
2. Pre-process the data
=======
####2. Pre-process the data####
>>>>>>> 7b09c48e908bd5c9ba7193e3b146f1ab39f9ed71

2.1. Apply pre-manipulations and pre-calculations to spreadsheets

2.2. Save spreadsheet files as comma-seperated value files (CSV)

2.3. Convert CSV-files to single XML databases for each year

<<<<<<< HEAD
3. Analysis
=======
####3. Analysis####
>>>>>>> 7b09c48e908bd5c9ba7193e3b146f1ab39f9ed71

3.1. Formulate pseudo-codes

3.2. Create XQuery's

3.3. Execute XQuery's

3.4. Interpret results

### How do I run the XQuery's? ###

Due to size restrictions, we omitted the initial .xml files within query repository (step 3.2 & 3.3)

* To run the programs smoothly one can simply include the document that is needed for the xquery
 (e.g. "2013-2014 XML_Excl_Teacher.xml") inside the local repository. The requested document
 can be found in the local repository Chapter 3.2\Section 3.2.2\Step 2.3\
* An alternative would be to change the file directory within the codes to for instance:
 "Chapter 3.2\Section 3.2.2\Step 2.3\XML Files\2013-2014 XML_Excl_teacher.xml"